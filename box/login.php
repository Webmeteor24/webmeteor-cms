<?php
/*-------------------------------------------------------+
| AhaScripte CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.ahascripte.de/
+--------------------------------------------------------+
| Filename: box/login.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if(!defined('IN_AHA')){ header('location: ../');}

if(logged_in()){
	$login= '<h2>Hallo '.$userdata['user_name'].'</h2>';
	$login.= '<form action="'.AHA_REQUEST.'" method="post">&nbsp;<input type="submit" name="logout" value="logout" class="button" />
    </form>';
	
}else{
	$login ='<h2>Login</h2>
	<p><form action="'.AHA_REQUEST.'" method="post">
    &nbsp;<input type="text" name="username" value="Username" />
    <input type="password" name="userpassword" value="Passwort"  />
    <input type="submit" name="login" value="login" class="button" />
    </form>
	&nbsp;<a href="'.METEOR_ROOT.'passwort_anfordern.html">Passwort vergessen</a></p>';
	
}
define ('LOGIN', $login);
?>