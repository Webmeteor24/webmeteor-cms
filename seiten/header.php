<?php 
/*-------------------------------------------------------+
| METEOR CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.meteor24.de/
+--------------------------------------------------------+
| Filename: seiten/header.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../../'); }
header('Cache-Control: must-revalidate, pre-check=0, no-store, no-cache, max-age=0, post-check=0');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta charset='UTF-8'>
 
 <meta http-equiv="pragma"  content="no-cache" />

<title><?php echo $settings['sitename'].' - '.$title ?></title>
<meta name='language' content='de,at,ch'>
<meta name="description" content="
<?php
if($meta_desc=='') echo $settings['meta_desc'];
else echo $meta_desc;
?>" />
<meta name="keywords" content="
<?php
if($keywords=='') echo $settings['keywords'];
else echo $keywords;
?>" />
<meta name="generator" content="METEOR CMS meteor24.de" />
<meta name="copyright" content="<?php echo $settings['sitename'] ?>" />
<meta name='robots' content='index, follow,noodp'>
<meta name="audience"                content="all"/>
<meta name="revisit-after"           content="1 days"/>

<link rel="stylesheet" href="<?php echo METEOR_ROOT.'design/'.$settings['design'] ?>/style.css" type="text/css" />

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo METEOR_ROOT ?>ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo METEOR_ROOT ?>ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo METEOR_ROOT ?>ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo METEOR_ROOT ?>ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo METEOR_ROOT ?>ico/-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo METEOR_ROOT ?>ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo METEOR_ROOT ?>ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo METEOR_ROOT ?>ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo METEOR_ROOT ?>ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo METEOR_ROOT ?>ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo METEOR_ROOT ?>ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo METEOR_ROOT ?>ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo METEOR_ROOT ?>ico/favicon-16x16.png">
<link rel="manifest" href="<?php echo METEOR_ROOT ?>ico//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo METEOR_ROOT ?>ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<?php 

echo $headtags; ?>
</head>

<body <?php echo $body ?>>