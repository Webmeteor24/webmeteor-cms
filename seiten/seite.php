<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: seiten/seite.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../'); }

if(isset($params[1]) && clean($params[1],"int")){
	$result=dbquery("SELECT * FROM ".DB_PAGE." WHERE page_id='".$params[1]."'");
	$pagedata=dbarray($result);

	$title=stripslashes($pagedata['page_title']);
	$content=stripslashes($pagedata['page_content']);
	$meta_desc=stripslashes($pagedata['page_meta_desc']);
	$keywords=stripslashes($pagedata['page_meta_key']);

	$headtags='';
	$body='';

}else header ('location: 404_'.$params[2]);

?>