<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: design/default/design_info.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../../index.html'); }
$box_positions=2;

// Auswahl Anzeige im Adminbereich
function show_design_position(){
	global $boxdata;
	
	echo '<input type="radio" name="box_anzeige" value="1" class="input"';
	if(isset($boxdata)){
		if($boxdata==1) echo 'selected';
	}
	echo '>Box 1</ br>';
	echo '<input type="radio" name="box_anzeige" value="2" class="input"';
	if(isset($boxdata)){
		if($boxdata==2) echo 'selected';
	}
	echo '>Box 2';
	
}
?>