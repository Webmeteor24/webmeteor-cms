<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: design/default/index.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../../'); }
header('Cache-Control: must-revalidate, pre-check=0, no-store, no-cache, max-age=0, post-check=0');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset='UTF-8'>

<title><?php echo $settings['sitename'].' - '.$title ?></title>
<meta name='language' content='de,at,ch'>
<meta name="description" content="
<?php
if($meta_desc=='') echo $settings['meta_desc'];
else echo $meta_desc;
?>" />
<meta name="keywords" content="
<?php
if($keywords=='') echo $settings['keywords'];
else echo $keywords;
?>" />
<meta name="generator" content="Meteor CMS www.webmeteor24.de" />
<meta name="copyright" content="<?php echo $settings['sitename'] ?>" />
<meta name='robots' content='index, follow,noodp'>
<meta name="audience"                content="all"/>
<meta name="revisit-after"           content="1 days"/>

<link rel="stylesheet" href="design/default/style.css" type="text/css" />

<link rel="apple-touch-icon" sizes="57x57" href="ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="ico/-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="ico/favicon-16x16.png">
<link rel="manifest" href="ico//manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="ico/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="design/default/js/modernizr-1.5.min.js"></script>
<?php 

echo $headtags; ?>
</head>

<body <?php echo $body ?>>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html"><?php echo $settings['sitename'] ?></a></h1>
          <h2><?php echo $settings['slogan'] ?></h2>
        </div>
      </div>
      <nav>
      <?php echo show_head_nav(' class="sf-menu" id="nav"'); ?>
      </nav>
    </header>
    <div id="site_content">
      <ul id="images">
        <li><img src="design/default/images/1.jpg" width="600" height="300" alt="seascape_one" /></li>
        <li><img src="design/default/images/2.jpg" width="600" height="300" alt="seascape_two" /></li>
        <li><img src="design/default/images/3.jpg" width="600" height="300" alt="seascape_three" /></li>
        <li><img src="design/default/images/4.jpg" width="600" height="300" alt="seascape_four" /></li>
        <li><img src="design/default/images/5.jpg" width="600" height="300" alt="seascape_five" /></li>
        <li><img src="design/default/images/6.jpg" width="600" height="300" alt="seascape_seascape" /></li>
      </ul>
      <div id="sidebar_container">
        <div class="sidebar">
        <?php echo LEFT_SITE.RIGHT_SITE; ?>
        </div>
      </div>
      <div class="content">
        <h1><?php echo $title ?></h1>
        <?php echo $content ?>
      </div>
    </div>
    <footer>
    <?php copyright(); ?>
    </footer>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="design/default/js/jquery.js"></script>
  <script type="text/javascript" src="design/default/js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="design/default/js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="design/default/js/jquery.kwicks-1.5.1.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#images').kwicks({
        max : 600,
        spacing : 2
      });
      $('ul.sf-menu').sooperfish();
    });
  </script>

<?php
function openbox($title){
	echo "<h3>".$title."</h3>\n";
}

function closebox(){
	
}
?>