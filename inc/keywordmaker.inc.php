<?php
/*-------------------------------------------------------+
| AhaScripte Mini CMS
| Copyright (C) 2009 Dennis Vorpahl
| http://www.ahascripte.de/
+--------------------------------------------------------+
| Filename: inc/keywordmaker.inc.php v1.0
| Author: Dennis Vorpahl (Comy)
+--------------------------------------------------------+
| Dieses Programm ist Copyright geschützt.
| Alle sichtbaren und unsichtbaren Copyright 
| Hinweise müssen erhalten bleiben!
| Werden diese entfernt, erlischt die Lizenz
| und dieses Script muss von sämtlichen Datenträgern
| gelöscht werden.
+--------------------------------------------------------*/

function makeCloud($gejoin, $mindSchrift = 12, $maxSchrift = 30)
{

$body =strip_tags($gejoin, '<body>');
//Umlaute und andere Sachen konvertieren
$replace = array("ä", "Ä", "ü", "Ü", "ö", "Ö", "ß", "Euro", " ", "und", " ", " ", " ", " ", " ");
$search = array("&auml;", "&Auml;", "&uuml;", "&Uuml;", "&ouml;", "&Ouml;", "&szlig;", "&euro;", "&quot;", "&amp;", "&nbsp;", "&ndash;", "&bdquo;", "&ldquo;", "&mdash;");
$body = str_replace($search, $replace, $body);
//Stopwörter, wie Marken usw. Filtern
$stop_words = array("lesen", "gelesen", "Zeilenumbr", "Dennis", "Tweet", "Kommentare", "August", "Online", "Nicht", "Werden", "einen");
$body= str_replace($stop_words, " ", $body);

$body = ucwords($body);
$freqWords = array();


    foreach(str_word_count($body, 1) as $word)
    {
		$laenge=strlen($word);
		if($laenge>4){
        if(array_key_exists($word, $freqWords))
        {$freqWords[$word]++;}
        else{$freqWords[$word]=0;}
		}
    }

arsort($freqWords);  //rückwärts sortiert, häufigste voran

$minCount = min(array_values($freqWords));
$maxCount = max(array_values($freqWords));
$sprd = $maxCount - $minCount;

$Bodycloud = array();
//Keywords nach Wortlänge aussortieren


$sprd == 0 && $sprd = 1;

    foreach( $freqWords as $tag => $kount )
     {

      // Häufigkeit, mehr als einmaliges Vorkommen
      if($kount>"1"){
      $Bodycloud[] = stripslashes($tag);
                    }
     }

shuffle($Bodycloud);
return join(", ", $Bodycloud);
} //endfunc
$meta_keyw = makeCloud(CONTENT);
set_meta('keywords', $meta_keyw);

$desc = strip_tags(CONTENT);
set_meta('description', trimlink($fusion_page_title.': '.$desc,200));
	
	add_to_head('<meta http-equiv="content-language" content="' . $locale['xml_lang'].'" />');
    add_to_head('<meta http-equiv="cache-control" content="no-cache" />');
	add_to_head('<meta name="robots" content="index,follow" />');
	add_to_head('<meta name="language" content="' . $locale['xml_lang'].'" />');
	add_to_head('<meta name="author" content="' . $settings['siteusername'].'" />');
	add_to_head('<meta name="publisher" content="' . $settings['siteusername'].'" />');
	add_to_head('<meta name="company" content="' . $settings['sitename'].'" />');
	//add_to_head('<meta name="page-topic" content="' . metaClean(META_TOPIC).'" />');
	//add_to_head('<meta name="reply-to" content="' . META_REPLY_TO.'" />');
	add_to_head('<meta name="distribution" content="global" />');
	add_to_head('<meta name="revisit-after" content="24hours" />');
	add_to_head('<meta name="robots" content="noodp" />');
	add_to_head('<meta name="generator" content="SEO for PHP-Fusion by Starunited.de">');
?>