<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: inc/db_defines.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
define("DB_USER", DB_PREFIX."user");
define("DB_NAVIGATION", DB_PREFIX."navigation");
define("DB_SAFE_PAGES", DB_PREFIX."safe_pages");
define("DB_PAGE", DB_PREFIX."page");
define("DB_KONTAKT", DB_PREFIX."kontakt");
define("DB_BOX", DB_PREFIX."box");
define("DB_SHOP_SESSION", DB_PREFIX."shop_session");
define("DB_SHOP_WARENKORB", DB_PREFIX."shop_warenkorb");
define("DB_SHOP_HAUPTGRUPPEN", DB_PREFIX."shop_hauptgruppen");
define("DB_SHOP_UNTERGRUPPEN", DB_PREFIX."shop_untergruppen");
define("DB_SHOP_ARTIKEL", DB_PREFIX."shop_artikel");
define("DB_SHOP_ZAHLARTEN", DB_PREFIX."shop_zahlarten");
define("DB_SHOP_BESTELLUNGEN", DB_PREFIX."shop_bestellungen");
define("DB_SHOP_VERSANDARTEN", DB_PREFIX."shop_versandarten");
define("DB_SETTINGS", DB_PREFIX."settings");
define("DB_RATINGS", DB_PREFIX."ratings");
define("DB_COMMENTS", DB_PREFIX."comments");
define("DB_USER_FIELD_CATS", DB_PREFIX."user_field_cats");
define("DB_USER_FIELDS", DB_PREFIX."user_fields");
define("DB_ERWEITERUNGEN", DB_PREFIX."erweiterung");
?>