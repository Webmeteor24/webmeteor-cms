<?php 
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2015 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: inc/admin_inc.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../index.html'); }

function showeditor($name){
	echo "<script type=\"text/javascript\">
				CKEDITOR.replace( '".$name."',{
					extraPlugins : 'uicolor',
					uiColor: '#1d1d1d',
					width: '430px'
				} );
			</script>\n";
	
}

function pageurl($seite,$id,$page_title){
	$umlaute = array("/&auml;/","/&ouml;/","/&uuml;/","/&Auml;/","/&Ouml;/","/&Uuml;/","/&szlig;/");
    $replace = array("ae","oe","ue","Ae","Oe","Ue","ss");
    $page_name = preg_replace($umlaute, $replace, $page_title);
	$page_name = str_replace(" ", "-", strtolower($page_name));
	$page_url= $seite.'_'.$id.'_'.$page_name.'.html';
	
}
?>