<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: inc/box.inc.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
if (!defined("IN_METEOR")) { header('location: ../'); }

function show_box($position){
	global $params;
	
	$seite=clean($params[0],'int');
	$result=dbquery("SELECT * FROM ".DB_BOX." WHERE box_anzeige='".$position."' AND box_page !LIKE '%".$seite."%' ORDER BY box_order");
	if($result){
		while($data=dbarray($result)){
			openbox($data['box_title']);
			if($data['box_url']!='' && file_exists(BOX.$data['box_url'])){
				require_once BOX.$data['box_url'];
			}else{
				echo $data['box_content'];
			}
			closebox();
		}
	}
	
}
?>