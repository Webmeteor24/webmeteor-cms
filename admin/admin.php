<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/index.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie k�nnen es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation ver�ffentlicht,
| weitergeben und/oder modifizieren,
| entweder gem�� Version 3 der Lizenz oder (nach Ihrer Option) jeder sp�teren Version.
|
| Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung,
| da� es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT F�R EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';
if (iADMIN){

$body='';
$headtags='';
$title='Admin&uuml;bersicht';

require_once 'secondheader.php';

?>

            <!--Main-Content-->
            
<?php
require_once 'footer.php';

}else{
	header('location: index.php');
}
?>