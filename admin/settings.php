<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/settings.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';
//check ob Admin und ob er die erforderlichen rechte hat
if (iADMIN){

$body='';
$headtags='';
$title='Haupteinstellungen';
require_once 'secondheader.php';
require_once '../inc/file.func.php';

if(isset($_POST['design'])){
   //$_POST eingaben absichern zum Abspeichern in die Datenbank
   $sitename=$_POST['site_name'];
   $siteemail=$_POST['site_email'];
   $siteowner=$_POST['site_owner'];
   $sitedesign=$_POST['design'];
   $siteslogan=$_POST['slogan'];
   $startpage=$_POST['startpage'];
   
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$sitename."' WHERE settings_name='sitename'");
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$siteemail."' WHERE settings_name='siteemail'");
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$siteowner."' WHERE settings_name='siteowner'");
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$sitedesign."' WHERE settings_name='design'");
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$siteslogan."' WHERE settings_name='slogan'");
   dbquery("UPDATE ".DB_SETTINGS." SET settings_wert='".$startpage."' WHERE settings_name='startpage'");
   
   unset($settings);
   $settings = array();
	$result = dbquery("SELECT settings_name, settings_wert FROM ".DB_SETTINGS);
	if ($result){
		while ($data = dbarray($result)){
			$settings[$data['settings_name']] = $data['settings_wert'];
		}
	}
}
   
//Designs auslesen und in Array speichern für die Design Auswahl   
$designs = makefilelist('../design/', ".|..", true, "folders");

?>
<h3 class="p1">Haupteinstellungen</h3>
              <form action='settings.php' name='settings' id="settings" method='post'>
                <fieldset id="form">
                  <label for="site_name"><span class="text-form">Name der Webseite:</span>
                    <input type="text" name="site_name" value="<? echo $settings['sitename']; ?>">
                  </label>
                  <label for="site_email"><span class="text-form">Email-Adresse:</span>
                    <input type="text" name="site_email" value="<? echo $settings['siteemail']; ?>">
                  </label>
                  <label for="site_owner"><span class="text-form">Ihr Name (wird nur in den E-Mails angezeigt):</span>
                    <input type="text" name="site_owner" value="<? echo $settings['siteowner']; ?>">
                  </label>
                  <label for="slogan"><span class="text-form">Slogan (wird nicht von allen Designs unterst&uuml;tzt):</span>
                    <input type="text" name="slogan" value="<? echo $settings['slogan']; ?>">
                  </label>
                  <label for="design"><span class="text-form">Seiten Design:</span>
                    <select name='design' class='input'>
                      <? echo makefileopts($designs,$settings['design']) ?>
                    </select>
                  </label>
                  <label for="startpage"><span class="text-form">Startseite:</span>
                    <input type="text" name="startpage" value="<? echo $settings['startpage']; ?>">
                  </label>
                  <div class="clear"></div>
                  <div class="buttons"> <a class="button" href="#" onClick="document.getElementById('settings').submit()">speichern</a> </div>
                </fieldset>
              </form>
 <?php
 require_once 'footer.php';
}else{
	header('location: index.php');
}
?>