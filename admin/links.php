<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/navs.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Navigation';

require_once 'secondheader.php';

if(!isset($_GET['action'])){ $_GET['action']='';}
switch($_GET['action']) {

    case "insert":
	Insert();
	break;
	
	case "bearbeiten":
	Bearbeiten(clean($_GET['id'],'int'));
	break;
	
	case "delete":
	Delete(clean($_GET['id'],'int'));
	break;
	
	case "order":
	Order(clean($_GET['id'],'int'));
	break;
	
	case "update":
	Update(clean($_GET['id'],'int'));
	break;
	
	default:
    Uebersicht();
	break;
}
require_once 'footer.php';

}else{
	header('location: index.php');
}

function Uebersicht(){
    
	$navdata = dbquery("SELECT * FROM ".DB_PREFIX."navigation ORDER BY nav_order ASC");
			
			echo "
 <table align='center' cellpadding='0' cellspacing='0' class='main' style='width:660px'>
<tr>
<td>Sortierung:</td>
<td>Webnav:</td>
<td>Optionen:</td>
</tr>

			";
			
			  while ($hnav = dbarray($navdata)){
				if($hnav['nav_url']!=''){
				        echo "<tr><form action='links.php?action=order&id=".$hnav['nav_id']."' name='nav_order_form' method='post'>";
					    echo "<td><input type='text' class='input' name='nav_order' value='".$hnav['nav_order']."' /></td>";
						echo "<td><a href='../".$hnav['nav_url']."' target='_blank' title='".$hnav['nav_name']."'>".$hnav['nav_name']."</a></td>";
						echo "<td><a href='links.php?action=bearbeiten&id=".$hnav['nav_id']."'>bearbeiten</a> &nbsp; <a href='links.php?action=delete&id=".$hnav['nav_id']."'>l&ouml;schen</a> &nbsp; <input type='submit' class='button' name='nav_order_form' value='Sortierung &auml;ndern' /></td>";
						echo "</form></tr>";
	                
			   }
			  }
			
			   echo "
			   </table>
			   ";
			   echo "<hr />";
			   echo "<form action='links.php?action=insert' name='nav_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Linkname:</td>
<td><input type='text' class='input' name='nav_name' value='' /></td>
</tr>
<tr>
<td>Linkurl:</td>
<td><input type='text' class='input' name='nav_url' value='' /></td>
</tr>
<tr>
<td>Linkreihenfolge:</td>
<td><input type='text' class='input' name='nav_order' value='' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='nav_form' value='nav anlegen' /></td>
</tr>
</table>
</form>";
}

function Insert(){
   dbquery("INSERT INTO ".DB_PREFIX."navigation (nav_name, nav_url, nav_order) VALUES ('".$_POST['nav_name']."', '".$_POST['nav_url']."', '".$_POST['nav_order']."')");
   header('location: links.php');
}

function Bearbeiten($id){
   $result=dbquery("SELECT * FROM ".DB_PREFIX."navigation WHERE nav_id='".$id."'");
   $navdata=dbarray($result);
   echo "<form action='links.php?action=update&id=".$id."' name='nav_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Linkname:</td>
<td><input type='text' class='input' name='nav_name' value='".$navdata['nav_name']."' /></td>
</tr>
<tr>
<td>Linkurl:</td>
<td><input type='text' class='input' name='nav_url' value='".$navdata['nav_url']."' /></td>
</tr>
<tr>
<td>Linkreihenfolge:</td>
<td><input type='text' class='input' name='nav_order' value='".$navdata['nav_order']."' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='nav_form' value='nav &auml;ndern' /></td>
</tr>
</table>
</form>";
}

function Delete($id){
   dbquery("DELETE FROM ".DB_PREFIX."navigation WHERE nav_id='".$id."'");
   header('location: links.php');
}

function Order($id){
   dbquery("UPDATE ".DB_PREFIX."navigation SET nav_order='".$_POST['nav_order']."' WHERE nav_id='".$id."'");
   header('location: links.php');
}

function Update($id){
   dbquery("UPDATE ".DB_PREFIX."navigation SET nav_order='".$_POST['nav_order']."' WHERE nav_id='".$id."'");
   dbquery("UPDATE ".DB_PREFIX."navigation SET nav_name='".$_POST['nav_name']."' WHERE nav_id='".$id."'");
   dbquery("UPDATE ".DB_PREFIX."navigation SET nav_url='".$_POST['nav_url']."' WHERE nav_id='".$id."'");
   header('location: links.php');
}
?>