<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/kontakt.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie k�nnen es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation ver�ffentlicht,
| weitergeben und/oder modifizieren,
| entweder gem�� Version 3 der Lizenz oder (nach Ihrer Option) jeder sp�teren Version.
|
| Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung,
| da� es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT F�R EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Seite erstellen';
require_once 'secondheader.php';

if(isset($_POST['page_title'])){
  //Daten validieren f�r den Datenbankeintrag
  $page_title = $_POST['page_title'];
  $page_content=$_POST['page_content'];
  $page_meta_desc=$_POST['page_meta_desc'];
  $page_keywords=$_POST['page_keywords'];
  // wenn bearbeitet
  if(isset($_GET['action']) && $_GET['action']=='update'){
      dbquery("UPDATE ".DB_PREFIX."page SET page_title='".$page_title."', page_content='".$page_content."', page_meta_desc='".$page_meta_desc."', page_meta_key='".$page_keywords."' WHERE page_id=".clean($_GET['id'],'int')."");
	  
	  $id=clean($_GET['id'],'int');
	  $umlaute = array("/&auml;/","/&ouml;/","/&uuml;/","/&Auml;/","/&Ouml;/","/&Uuml;/","/&szlig;/");
      $replace = array("ae","oe","ue","Ae","Oe","Ue","ss");
      $page_name = preg_replace($umlaute, $replace, $page_title);
	  $page_name = str_replace(" ", "_", strtolower($page_name));
	  $page_url='seite_'.$id.'_'.$page_name.'.html';
	  $result=dbquery("SELECT nav_id FROM ".DB_PREFIX."navigation WHERE nav_url='$page_url'");
	  if(!$result){
		  dbquery("INSERT INTO ".DB_PREFIX."navigation (nav_name, nav_url) VALUES ('".$page_name."', '".$page_url."')");
	  }
  }
  //wenn neu
  if(isset($_GET['action']) && $_GET['action']=='insert'){
      $insert = dbquery("INSERT INTO ".DB_PREFIX."page (page_title, page_content, page_meta_desc, page_meta_key) VALUES ('".$page_title."', '".$page_content."', '".$page_meta_desc."', '".$page_keywords."')");
	  
	  if(isset($_POST['nav']) && $_POST['nav']=='1'){
	     $id=db_insert_id();
		 //URL f�r das Web konvertieren
				$umlaute = array("/&auml;/","/&ouml;/","/&uuml;/","/&Auml;/","/&Ouml;/","/&Uuml;/","/&szlig;/");
                $replace = array("ae","oe","ue","Ae","Oe","Ue","ss");
                $page_name = preg_replace($umlaute, $replace, $page_title);
				$page_name = str_replace(" ", "_", strtolower($page_name));
		 $page_url='seite_'.$id.'_'.$page_name.'.html';
	     dbquery("INSERT INTO ".DB_PREFIX."navigation (nav_name, nav_url) VALUES ('".$page_title."', '".$page_url."')");
		}
	}
}

if(isset($_GET['action'])&& $_GET['action']=='bearbeiten'){
	if(isset($_POST['delete'])){
		dbquery("DELETE FROM ".DB_PREFIX."page WHERE page_id='".clean($_POST['page_id'],'int')."'");
		header('location: seite.php');
	}else{
   $result=dbquery("SELECT page_id, page_title,page_content, page_meta_desc, page_meta_key FROM ".DB_PREFIX."page WHERE page_id='".clean($_POST['page_id'],'int')."'");
   $pagedata=dbarray($result);
   
?>
<h3 class="p1">Seite erstellen Admin</h3>
  <form action='seite.php?action=update&id=<?php echo $pagedata['page_id'] ?>' name='page_form' method='post'>
    <table align='center' cellpadding='0' cellspacing='0'>
      <tr>
        <td width='30%'>&Uuml;berschrift:</td>
        <td width='30%'><input type='text' name='page_title' class='input' value='<? echo stripslashes($pagedata['page_title']) ?>' /></td>
      </tr>
      <tr>
<td width='30%' valign='top' colspan="2">Content:</td>
</tr>
<tr>
<td colspan="2"><textarea name='page_content' class='input' cols='95' rows='10' style='width:98%'><? echo stripslashes($pagedata['page_content']) ?></textarea></td>
<script type="text/javascript">
				CKEDITOR.replace( 'page_content',{
					extraPlugins : 'uicolor',
					uiColor: '#1d1d1d',
					width: '430px'
				} );
			</script>
</tr>
      <tr>
        <td width='30%'>Meta Beschreibung:</td>
        <td width='30%'><input type='text' name='page_meta_desc' class='input' value='<? echo stripslashes($pagedata['page_meta_desc']) ?>' /></td>
      </tr>
      <tr>
        <td width='30%'>Keywords:</td>
        <td width='30%'><input type='text' name='page_keywords' class='input' value='<? echo stripslashes($pagedata['page_meta_key']) ?>' /></td>
      </tr>
      <tr>
        <td width='30%'>Zur Navigation hinzuf&uuml;gen:</td>
        <td width='30%'><input type='checkbox' name='nav' class='input' value='1' /></td>
      </tr>
      <tr>
        <td colspan="2"><input type='submit' name='pageform' class='button' value='speichern' /></td>
      </tr>
    </table>
  </form>
  <?php 
	}
}else{
	$result=dbquery("SELECT * FROM ".DB_PREFIX."page");
	$editlist = "";
		while ($data = dbarray($result)) {
			$editlist .= "<option value='".$data['page_id']."'>".$data['page_title']."</option>\n";
		}

	  echo "<div style='text-align:center; width:600px;'>\n<form name='selectform' method='post' action='seite.php?action=bearbeiten'>\n";
		echo "<select name='page_id' class='input' style='width:200px;'>\n".$editlist."</select>\n";
		echo "<input type='submit' name='edit' value='bearbeiten' class='button' />\n";
		echo "<input type='submit' name='delete' value='l&ouml;schen' class='button' />\n";
		echo "</form>\n</div>\n<br /><br />";
		?>
        <form action='seite.php?action=insert' name='page_form' method='post'>
          <table align='center' cellpadding='0' cellspacing='0'>
            <tr>
              <td width='30%'>&Uuml;berschrift:</td>
              <td width='30%'><input type='text' name='page_title' class='input' value='' /></td>
            </tr>
            <tr>
<td width='30%' valign='top' colspan="2">Content:</td>
</tr>
<tr>
<td colspan="2"><textarea name='page_content' class='input' cols='95' rows='10' style='width:98%'></textarea></td>
<script type="text/javascript">
				CKEDITOR.replace( 'page_content',{
					extraPlugins : 'uicolor',
					uiColor: '#1d1d1d',
					width: '430px',
				} );
			</script>
</tr>
            <tr>
              <td width='30%'>Meta Beschreibung:</td>
              <td width='30%'><input type='text' name='page_meta_desc' class='input' value='' /></td>
            </tr>
            <tr>
              <td width='30%'>Keywords:</td>
              <td width='30%'><input type='text' name='page_keywords' class='input' value='' /></td>
            </tr>
            <tr>
              <td width='30%'>Zur Navigation hinzuf&uuml;gen:</td>
              <td width='30%'><input type='checkbox' name='nav' class='input' value='1' /></td>
            </tr>
            <tr>
              <td colspan="2"><input type='submit' name='pageform' class='button' value='speichern' /></td>
            </tr>
          </table>
        </form>
        <?php 
}
require_once 'footer.php';
}else{
	header('location: index.php');
}
?>