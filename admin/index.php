<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/index.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';


if(iADMIN){
	header('location: admin.php');
}else{

   	$title='Meteor CMS Adminbereich';
   	$body='';
	$headtags='';
	require_once '../seiten/header_mce.php';

?>
<div class="extra">
  <div class="main"> 
    <!--==============================header=================================-->
    <header>
      <div class="wrapper p4">
        <h1><a href="">Meteor CMS Adminbereich</a></h1>
      </div>
      <nav>
        <ul class="menu">
          <li><a href="http://www.webmeteor24.de" target="_blank">Meteor Home</a></li>
          <li><a href="http://www.webdesign-mittelholstein.de/kontakt.html" target="_blank">Installationsservice</a></li>
          <li><a href="http://www.webmeteor24.de/download" target="_blank">Downloads</a></li>
          <li class="last"><a href="http://www.webmeteor24.de/forum" target="_blank">Forum</a></li>
        </ul>
      </nav>
    </header>
    <!--==============================content================================-->
    <section id="content">
      <div class="container_12">
        <div class="wrapper">
          <article class="grid_8">
            <div class="indent-left2 indent-top">
              <h3 class="p1">Login</h3>
              <form id="login" action='index.php' name='adminlogin' method='post'>
              <fieldset id="form">
                  <label><span class="text-form">Benutzername:</span>
                    <input type="text" name="user_name"></label>
                  <label><span class="text-form">Passwort:</span>
                    <input type="password" name="user_password"></label>
                <div class="clear"></div>
                  <div class="buttons"><a class="button" href="#" onClick="document.getElementById('login').submit()">Login</a> </div>
                </fieldset>
              </form>
            </div>
          </article>
          <article class="grid_4">
            <div class="box-2">
              <div class="padding">
                <h3>Sorry!</h3>
                <p class="border-bot p2">Sie möchten dieses CMS auch? <br>
                  Dann haben wir eine gute Nachricht.<br>
                  Sie können es bei <a href="http://www.webmeteor24.de" target="_blank">WebMeteor24</a> downloaden.</p>
                <h3>Unsere Empfehlungen</h3>
                <p class="border-bot p2">Meteor CMS ist getestet auf dem kostenlosen Webspace von <a href="http://www.lima-city.de/homepage/ref:268057" target="_blank">Lima-City</a></p>
              </div>
            </div>
          </article>
        </div>
      </div>
      <div class="block"></div>
    </section>
  </div>
</div>
<!--==============================footer=================================-->
<footer>
  <div class="main">
    <div class="inner">
      <p>WebMeteor24 &copy; 2015 </p>
      <p>Meteor CMS ist lizensiert unter <a class="link" href="http://www.gnu.org/licenses/gpl.html" target="_blank" rel="nofollow">GNU General Public License Version 3</a></p>
    </div>
  </div>
</footer>
<script type="text/javascript"> Cufon.now(); </script>
</body></html><?php
}
?>