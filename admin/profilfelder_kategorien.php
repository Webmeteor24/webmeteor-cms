<?php
/*-------------------------------------------------------+
| Meteor CMS
| Copyright (C) 2012 Dennis Vorpahl
| http://www.webmeteor24.de/
+--------------------------------------------------------+
| Filename: admin/profilfelder_kategorien.php v1.0
| Author: Dennis Vorpahl
+--------------------------------------------------------+
| Dieses Programm ist freie Software.
| Sie können es unter den Bedingungen der GNU General Public License,
| wie von der Free Software Foundation veröffentlicht,
| weitergeben und/oder modifizieren,
| entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
|
| Die Veröffentlichung dieses Programms erfolgt in der Hoffnung,
| daß es Ihnen von Nutzen sein wird,
| aber OHNE IRGENDEINE GARANTIE,
| sogar ohne die implizite Garantie der MARKTREIFE
| oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK.
| Details finden Sie in der GNU General Public License.
|
| Sie sollten ein Exemplar der GNU General Public License
| zusammen mit diesem Programm erhalten haben.
| Falls nicht, siehe <http://www.gnu.org/licenses/>.
+--------------------------------------------------------*/
require_once '../main.php';

if (iADMIN){

$body='';
$headtags='';
$title=' - Profilfelder Kategorien';

require_once 'secondheader.php';

if(!isset($_GET['action'])){ $_GET['action']='';}
switch($_GET['action']) {

    case "insert":
	Insert();
	break;
	
	case "bearbeiten":
	Bearbeiten(clean($_GET['id'],'int'));
	break;
	
	case "delete":
	Delete(clean($_GET['id'],'int'));
	break;
	
	case "order":
	Order(clean($_GET['id'],'int'));
	break;
	
	case "update":
	Update(clean($_GET['id'],'int'));
	break;
	
	default:
    Uebersicht();
	break;
}
require_once 'footer.php';

}else{
	header('location: index.php');
}

function Uebersicht(){
    
	$result = dbquery("SELECT * FROM ".DB_PREFIX."user_field_cats ORDER BY field_cat_order ASC");
			
			echo "
 <table align='center' cellpadding='0' cellspacing='0' class='main' style='width:660px'>
<tr>
<td>Sortierung:</td>
<td>Kategorie:</td>
<td>Optionen:</td>
</tr>

			";
			
			  while ($cat = dbarray($result)){
				if($cat['field_cat_id']!=''){
				        echo "<tr><form action='links.php?action=order&id=".$cat['field_cat_id']."' name='fieldcat_order_form' method='post'>";
					    echo "<td><input type='text' class='input' name='field_cat_order' value='".$cat['field_cat_order']."' /></td>";
						echo "<td>".$cat['field_cat_name']."</a></td>";
						echo "<td><a href='profilfelder_kategorien.php?action=bearbeiten&id=".$cat['field_cat_id']."'>bearbeiten</a> &nbsp; <a href='profilfelder_kategorien.php?action=delete&id=".$cat['field_cat_id']."'>l&ouml;schen</a> &nbsp; <input type='submit' class='button' name='fieldcat_order_form' value='Sortierung &auml;ndern' /></td>";
						echo "</form></tr>";
	                
			   }
			  }
			
			   echo "
			   </table>
			   ";
			   echo "<hr />";
			   echo "<form action='profilfelder_kategorien.php?action=insert' name='field_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Kategoriename:</td>
<td><input type='text' class='input' name='fieldcat_name' value='' /></td>
</tr>
<tr>
<td>Kategorienreihenfolge:</td>
<td><input type='text' class='input' name='fieldcat_order' value='' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='cat_form' value='Kategorie anlegen' /></td>
</tr>
</table>
</form>";
}

function Insert(){
   dbquery("INSERT INTO ".DB_PREFIX."user_field_cats (field_cat_name, field_cat_order) VALUES ('".$_POST['fieldcat_name']."', '".$_POST['fieldcat_order']."')");
   header('location: profilfelder_kategorien.php');
}

function Bearbeiten($id){
   $result=dbquery("SELECT * FROM ".DB_PREFIX."user_field_cats WHERE field_cat_id='".$id."'");
   $catdata=dbarray($result);
   echo "<form action='profilfelder_kategorien.php?action=update&id=".$id."' name='cat_form' method='post'>
 <table align='center' cellpadding='0' cellspacing='0' class='main'>
<tr>
<td>Kategoriename:</td>
<td><input type='text' class='input' name='cat_name' value='".$catdata['field_cat_name']."' /></td>
</tr>
<tr>
<td>Kategorienreihenfolge:</td>
<td><input type='text' class='input' name='cat_order' value='".$navdata['field_cat_order']."' /></td>
</tr>
<tr>
<td colspan='2' align='center'><input type='submit' class='button' name='nav_form' value='Kategorie &auml;ndern' /></td>
</tr>
</table>
</form>";
}

function Delete($id){
   dbquery("DELETE FROM ".DB_PREFIX."user_field_cats WHERE nav_id='".$id."'");
   header('location: profilfelder_kategorien.php');
}

function Order($id){
   dbquery("UPDATE ".DB_PREFIX."user_field_cats SET field_cat_order='".$_POST['field_cat_order']."' WHERE nav_id='".$id."'");
   header('location: profilfelder_kategorien.php');
}

function Update($id){
   dbquery("UPDATE ".DB_PREFIX."user_field_cats SET field_cat_order='".$_POST['cat_order']."' WHERE nav_id='".$id."'");
   dbquery("UPDATE ".DB_PREFIX."user_field_cats SET nav_name='".$_POST['cat_name']."' WHERE nav_id='".$id."'");
   header('location: profilfelder_kategorien.php');
}
?>